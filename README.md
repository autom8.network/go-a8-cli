# The a8 CLI

`a8 --help`

```
NAME:
   a8 - cli for accessing the autom(8) network

USAGE:
   a8 [global options] command [command options] [arguments...]

VERSION:
   0.0.1

COMMANDS:
     apps     list apps
     fns      list functions for a given app
     fn       gets a single functions detail
     invoke   invokes a function
     build    runs builds docker container
     bump     bumps fn version
     update   updates the fn information in DB
     deploy   deploys function to app
     init     creates a blank app
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --verbose, -b  if set to true will print out details cli logging
   --help, -h     show help
   --version, -v  print the version
```