#!/bin/sh
set -e

# Install script to install a8

command_exists() {
  command -v "$@" > /dev/null 2>&1
}

case "$(uname -m)" in
  *64)
    ;;
  *)
    echo >&2 'Error: you are not using a 64bit platform.'
    echo >&2 'Functions CLI currently only supports 64bit platforms.'
    exit 1
    ;;
esac

user="$(id -un 2>/dev/null || true)"

sh_c='sh -c'
if [ "$user" != 'root' ]; then
  if command_exists sudo; then
    sh_c='sudo -E sh -c'
  elif command_exists su; then
    sh_c='su -c'
  else
    echo >&2 'Error: this installer needs the ability to run commands as root.'
    echo >&2 'We are unable to find either "sudo" or "su" available to make this happen.'
    exit 1
  fi
fi

curl=''
if command_exists curl; then
  curl='curl -sSL -o'
elif command_exists wget; then
  curl='wget -qO'
elif command_exists busybox && busybox --list-modules | grep -q wget; then
  curl='busybox wget -qO'
else
    echo >&2 'Error: this installer needs the ability to run wget or curl.'
    echo >&2 'We are unable to find either "wget" or "curl" available to make this happen.'
    exit 1
fi

url='https://s3.amazonaws.com/a8clirepository'
version='0.2.5'

# perform some very rudimentary platform detection
case "$(uname)" in
  Linux)
    $sh_c "$curl /tmp/a8_linux $url/linux-amd-64/$version/a8"
    $sh_c "mv /tmp/a8 /usr/local/bin/a8"
    $sh_c "chmod +x /usr/local/bin/a8"
    a8 --version
    ;;
  Darwin)
    $sh_c "$curl /tmp/a8_mac $url/osx/$version/a8"
    $sh_c "mv /tmp/a8_mac /usr/local/bin/a8"
    $sh_c "chmod +x /usr/local/bin/a8"
    a8 --version
    ;;
  WindowsNT)
    
    ;;
  *)
    cat >&2 <<'EOF'
  Either your platform is not easily detectable or is not supported by this
  installer script (yet - PRs welcome!).
EOF
    exit 1
esac

cat >&2 <<'EOF'

            .o  .ooooo.   o.   
           .8' d88'   `8. `8.  
 .oooo.   .8'  Y88..  .8'  `8. 
`P  )88b  88    `88888b.    88 
 .oP"888  88   .8'  ``88b   88 
d8(  888  `8.  `8.   .88P  .8' 
`Y888""8o  `8.  `boood8'  .8'  
            `"            "' 

EOF
exit 0