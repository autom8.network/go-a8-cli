module gitlab.com/autom8.network/go-a8-cli

go 1.12

require (
	github.com/Microsoft/go-winio v0.4.12 // indirect
	github.com/docker/distribution v0.0.0-20190214141510-6d62eb1d4a3515399431b713fde3ce5a9b40e8d5 // indirect
	github.com/docker/docker v0.0.0-20190214141510-818d0dc5fda4866032ff020ad1514a147d718bf9
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.3.3 // indirect
	github.com/fatih/color v1.7.0
	github.com/fnproject/cli v0.0.0-20190308110852-b317f721f6ea
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/morikuni/aec v0.0.0-20170113033406-39771216ff4c // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible // indirect
	github.com/sirupsen/logrus v1.4.1
	github.com/urfave/cli v1.20.0
	gitlab.com/autom8.network/go-a8-http v0.0.0-20190618141447-2303c420574e
	gitlab.com/autom8.network/go-a8-util v0.0.0-20190618141334-8335fc834c3f
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	google.golang.org/grpc v1.19.0 // indirect
	gopkg.in/yaml.v2 v2.2.2
	gotest.tools v2.2.0+incompatible // indirect
)

// replace gitlab.com/autom8.network/go-a8-util => ../go-a8-util

replace gitlab.com/autom8.network/go-a8-http => ../go-a8-http
