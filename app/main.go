package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
	a8cli "gitlab.com/autom8.network/go-a8-cli/cli"
	cliUtil "gitlab.com/autom8.network/go-a8-cli/util"
	a8Util "gitlab.com/autom8.network/go-a8-util"
)

func main() {
	app := cli.NewApp()
	app.Name = "a8"
	app.Version = "0.2.4"
	app.Usage = "cli for accessing the autom(8) network"

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "verbose,b",
			Usage:       "if set to true will print out details cli logging",
			Destination: &cliUtil.Verbose,
		},
	}

	app.Commands = []cli.Command{
		cli.Command{
			Name:      "show",
			Usage:     "shows important information",
			ArgsUsage: "[data to show]",
			Subcommands: []cli.Command{
				cli.Command{
					Name:      "ns",
					Usage:     "displays data about a namespace, either child entires or detailed information",
					ArgsUsage: "[namespace]",
					Action:    a8cli.ShowNamespace,
				},
			},
		},
		cli.Command{
			Name:      "create",
			Usage:     "create various objects",
			ArgsUsage: "[data to show]",
			Subcommands: []cli.Command{
				cli.Command{
					Name:  "account",
					Usage: "creates a user account for access to the system",
					Flags: []cli.Flag{
						cli.StringFlag{Name: "email"},
						cli.StringFlag{Name: "handle"},
					},
					Action: a8cli.CreateAccount,
				},
			},
		},
		cli.Command{
			Name:      "login",
			Usage:     "log in to an account",
			Flags: []cli.Flag{
				cli.StringFlag{Name: "email"},
				cli.StringFlag{Name: "handle"},
			},
			Action: a8cli.SwitchAccount,
		},
		cli.Command{
			Name:      "invoke",
			Usage:     "invokes a function",
			ArgsUsage: "[fullyQualifiedName] [input]",
			Action:    a8cli.InvokeAction,
		},
		cli.Command{
			Name:   "build",
			Usage:  "builds the function image",
			Action: a8cli.BuildAction,
		},
		cli.Command{
			Name:  "bump",
			Usage: "bumps fn version",
			Flags: []cli.Flag{
				cli.BoolFlag{Name: "major"},
				cli.BoolFlag{Name: "minor"},
			},
			Action: a8cli.Bump,
		},
		cli.Command{
			Name:   "deploy",
			Usage:  "deploys a function",
			Action: a8cli.DeployAction,
		},
		cli.Command{
			Name:  "init",
			Usage: "creates a blank app",
			Flags: []cli.Flag{
				cli.IntFlag{Name: "timeout", Value: 30},
				cli.IntFlag{Name: "idle-timeout", Value: 30},
				cli.IntFlag{Name: "memory", Value: 128},
				cli.StringFlag{Name: "runtime", Value: "node"},
				cli.StringFlag{Name: "folder"},
				cli.BoolFlag{Name: "slack"},
				cli.BoolFlag{Name: "discord"},
				cli.BoolFlag{Name: "sheets"},
			},
			ArgsUsage: "[functionName]",
			Action:    a8cli.InitAction,
		},
	}

	////set the log level before running any cli command
	app.Before = func(c *cli.Context) error {

		if !cliUtil.Verbose {
			// Error is saying it will show all Error and Fatal level messages, but not lower
			a8Util.SetLogLevel(a8Util.Error)
		}

		a8Util.Log.Info("starting ClI")

		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Println(err)
	}
}
