package util

import (
	dockerClient "github.com/docker/docker/client"
)

// DockerCli is how we interact with the docker daemon
var DockerCli, _ = dockerClient.NewClientWithOpts(dockerClient.FromEnv, dockerClient.WithVersion("1.39"))
