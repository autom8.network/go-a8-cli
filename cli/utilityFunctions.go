package cli

import (
	"crypto/rsa"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/autom8.network/go-a8-util/fqn"
	"io/ioutil"
	"net/mail"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"text/tabwriter"
	"time"

	cliUtil "gitlab.com/autom8.network/go-a8-cli/util"
	daemonClient "gitlab.com/autom8.network/go-a8-http/client"
	a8Util "gitlab.com/autom8.network/go-a8-util"
	a8crypto "gitlab.com/autom8.network/go-a8-util/crypto"
	pb "gitlab.com/autom8.network/go-a8-util/proto"
	"gopkg.in/yaml.v2"
)

var errNoAccount = errors.New("👉 Please create an account first \n a8 create account --email <email address> --handle <handle>")

var configDir = ".a8"

var configFile = "config.yaml"

type fnConfig struct {
	SchemaVersion int                    `yaml:"schema_version"`
	Name          string                 `yaml:"name"`
	Version       string                 `yaml:"version"`
	Runtime       string                 `yaml:"runtime"`
	EntryPoint    string                 `yaml:"entrypoint"`
	Format        string                 `yaml:"format"`
	Timeout       int32                  `yaml:"timeout"`
	IdleTimeout   int32                  `yaml:"idle_timeout"`
	Memory        int32                  `yaml:"memory"`
	BuildImage    string                 `yaml:"build_image,omitempty"`
	RunImage      string                 `yaml:"run_image,omitempty"`
	ContentType   string                 `yaml:"content_type,omitempty" json:"content_type,omitempty"`
	Type          string                 `yaml:"type,omitempty" json:"type,omitempty"`
	Path          string                 `yaml:"path,omitempty" json:"path,omitempty"`
	Annotations   map[string]interface{} `yaml:"annotations,omitempty" json:"annotations,omitempty"`
	Config        map[string]string      `yaml:"config,omitempty" json:"config,omitempty"`
	Cpus          string                 `yaml:"cpus,omitempty" json:"cpus,omitempty"`
}

type a8Config struct {
	Account *a8Account `yaml:"account"`
}

type a8Account struct {
	Email      string `yaml:"email"`
	Handle     string `yaml:"handle"`
	PrivateKey string `yaml:"private_key"`
}

func getConfigsFromYaml() fnConfig {
	var config fnConfig

	yamlFile, err := ioutil.ReadFile("func.yaml")

	if err != nil {
		a8Util.Log.Fatal(fmt.Sprintf("yamlFile.Get err   #%v ", err))
	}
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		a8Util.Log.Fatal(fmt.Sprintf("Unmarshal: %v", err))
	}

	return config
}

func newA8Config(email string, handle string, pkPemPath string) (*a8Config, error) {
	_, err := mail.ParseAddress(email)
	if err != nil {
		return nil, errors.New("Please include a properly formatted email using --email")
	}

	if err = fqn.CheckLength(handle); err != nil {
		return nil, errors.New("Handle " + err.Error())
	}

	if err = fqn.CheckCharacters(handle); err != nil {
		return nil, errors.New("Handle " + err.Error())
	}

	return &a8Config{
		&a8Account{
			Email:      email,
			Handle:     handle,
			PrivateKey: pkPemPath,
		},
	}, nil
}

func a8configFilePath() (string, error) {
	file := configFile

	a8dir, err := a8FolderPath()
	if err != nil {
		return "", err
	}

	path := filepath.Join(a8dir, file)
	return path, nil
}

func a8PkPemFilePath(handle string) (string, error) {
	file := fmt.Sprintf("%s.pem", handle)

	a8dir, err := a8FolderPath()
	if err != nil {
		return "", err
	}

	path := filepath.Join(a8dir, file)
	return path, nil
}

//this finds the .a8 folder file path, and if it doesn't exist, creates it
func a8FolderPath() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}

	dir := configDir

	path := filepath.Join(usr.HomeDir, dir)

	if _, err := os.Stat(path); os.IsNotExist(err) {
		if err := os.MkdirAll(path, os.ModePerm); err != nil {
			return "", err
		}
	}

	if err != nil {
		return "", err
	}

	return path, nil
}

func createAccountFile(a8Account *a8Config, path string) error {
	newConfigBytes, err := yaml.Marshal(a8Account)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(path, newConfigBytes, 0644)
}

func getA8ConfigFromFile(path string) (*a8Config, error) {
	var config a8Config

	if !exists(path) {
		return nil, errNoAccount
	}

	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func getPrivateKeyFromFile(path string) (*rsa.PrivateKey, error) {
	pemFile, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return a8crypto.PemToPrivateKey(pemFile)
}

//bumpFnVersion, input is the lable, 0 = major, 1= minor, 2= patch
func bumpFnVersion(version int) (string, error) {

	config := getConfigsFromYaml()

	currentVersion := config.Version

	versionArray := strings.Split(currentVersion, ".")

	versionPart, _ := strconv.ParseInt(versionArray[version], 10, 64)

	versionPart++

	stringVer := fmt.Sprint(versionPart)

	versionArray[version] = stringVer

	newVersion := strings.Join(versionArray, ".")

	config.Version = newVersion

	newConfigBytes, _ := yaml.Marshal(config)

	err := ioutil.WriteFile("func.yaml", newConfigBytes, 0644)
	if err != nil {
		return "", err
	}

	return newVersion, nil
}

func update(configs fnConfig, imageName string) error {
	fnData, err := daemonClient.DeployFn(configs.Name, imageName, configs.Timeout, configs.IdleTimeout, configs.Memory)
	if err != nil {
		return err
	}

	if cliUtil.Verbose {
		if err = prettyPrintJson(fnData); err != nil {
			return err
		}
	}

	return nil
}

// exists checks if a file exists
func exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func displayFnList(namespaceList []*pb.NamespaceData) error {
	w := tabwriter.NewWriter(os.Stdout, 0, 8, 1, '\t', 0)
	_, err := fmt.Fprint(w, "Name", "\n")
	if err != nil {
		return err
	}
	for _, nsData := range namespaceList {
		name := nsData.NamespaceName
		_, err := fmt.Fprint(w, name, "\n")
		if err != nil {
			return err
		}
	}
	if err := w.Flush(); err != nil {
		return err
	}

	return nil
}

//prety prints an interface that has JSON types
func prettyPrintJson(fnDetail interface{}) error {
	prettyJson, err := json.MarshalIndent(fnDetail, "", "\t")
	if err != nil {
		return err
	}

	_, err = os.Stdout.Write(prettyJson)
	//add a new line as well
	fmt.Println()

	return err
}

func indicator(shutdownCh <-chan struct{}) {
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			fmt.Print(".")
		case <-shutdownCh:
			return
		}
	}
}

func printInvokeOutput(output *pb.InvokeResponse) {
	var err error

	switch output.Type {
	case "application/json":
		err = printInvokeJson(output.Data)
	default:
		err = printInvokeJson(output.Data)
	}

	if err != nil {
		fmt.Printf("output of type %s could not be formatted properly.", output.Type)
		fmt.Printf("error recieved from formatter: %s.", err)
		fmt.Printf("partial data dump of output data: %s.", a8Util.TruncateString(string(output.Data), 100))
	}

	return
}

func printInvokeJson(rawData []byte) error {
	var dat map[string]interface{}

	if err := json.Unmarshal(rawData, &dat); err != nil {
		return err
	}
	b, _ := json.MarshalIndent(dat, "", "  ")

	fmt.Print(string(b), "\n")

	return nil
}

func setupDaemonClient() error {
	path, err := a8configFilePath()
	if err != nil {
		return err
	}

	a8Config, err := getA8ConfigFromFile(path)
	if err != nil {
		return err
	}

	pk, err := getPrivateKeyFromFile(a8Config.Account.PrivateKey)

	if err != nil {
		return err
		//fmt.Printf("%s\n", err)
		//
		////we hard exit here to that way the error doesn't go to the CLI and trigger the help command
		//os.Exit(1)
	}

	config := daemonClient.NewDaemonConfig("", "", 0, pk)

	daemonClient.Config = &config

	return nil
}

//writes a8 configs to files as necessary
func writeConfigs(email string, handle string, pkPem []byte) error {
	pemPath, err := a8PkPemFilePath(handle)
	if err != nil {
		return err
	}

	if err := ioutil.WriteFile(pemPath, pkPem, 0644); err != nil {
		return err
	}

	a8Config, err := newA8Config(email, handle, pemPath)
	if err != nil {
		return err
	}

	path, err := a8configFilePath()
	if err != nil {
		return err
	}

	err = createAccountFile(a8Config, path)
	if err != nil {
		fmt.Printf(" error trying to create account %s \n ", err)
		return nil
	}

	return nil
}
