package cli

import (
	"bytes"
	"crypto/rsa"
	"crypto/x509"
	a8Crypto "gitlab.com/autom8.network/go-a8-util/crypto"
	"os"
	"path/filepath"
	"testing"
)

var password = "Password!"
var email = "s@s.com"
var handle = "sss"

var a8dir, _ = a8FolderPath()

func TestStructAndUnStruct(t *testing.T) {
	pkPem, _, pubBytes, err := a8Crypto.CreateEncryptedPK(password)
	if err != nil {
		t.Errorf("err newA8Config %s", err)
	}

	a8Config, err := newA8Config(email, handle, pkPem)
	if err != nil {
		t.Errorf("err newA8Config %s", err)
	}

	pk, err := a8Crypto.PemToPrivateKey(a8Config.Account.PrivateKeys[0])
	if err != nil {
		t.Errorf("err PemToPrivateKey %s", err)
	}

	pub, err := a8Crypto.PemToPublicKey(pubBytes)
	if err != nil {
		t.Errorf("err PemToPublicKey %s", err)
	}

	if !checkEqualPub(&pk.PublicKey, pub) {
		t.Errorf("err checkEqualPub, keys don't match!")

	}
}

func TestWriteAndReadPKFromFile(t *testing.T) {
	pkPem, _, pubBytes, err := a8Crypto.CreateEncryptedPK(password)
	if err != nil {
		t.Errorf("err newA8Config %s", err)
	}

	a8Config, err := newA8Config(email, handle, pkPem)
	if err != nil {
		t.Errorf("err newA8Config %s", err)
	}

	path := filepath.Join(a8dir, "test.yaml")
	defer os.Remove(path)

	err = createAccountFile(a8Config, path)
	if err != nil {
		t.Errorf("err PemToPublicKey %s", err)
	}

	a8ConfigRead, err := getA8ConfigFromFile(path)
	if err != nil {
		t.Errorf("err getA8ConfigFromFile %s", err)
	}

	if len(a8ConfigRead.Account.PrivateKeys) == 0 {
		t.Errorf("not private key array happened")
	}

	pkReadBytes := a8ConfigRead.Account.PrivateKeys[0]

	pk, err := a8Crypto.PemToPrivateKey(pkReadBytes)
	if err != nil {
		t.Errorf("err PemToPrivateKey %s", err)
	}

	pub, err := a8Crypto.PemToPublicKey(pubBytes)
	if err != nil {
		t.Errorf("err PemToPublicKey %s", err)
	}

	if !checkEqualPub(&pk.PublicKey, pub) {
		t.Errorf("public keys should be the same")
	}
}

func TestFileWriteReadAgain(t *testing.T) {
	pkPem, _, pubBytes, err := a8Crypto.CreateEncryptedPK(password)
	if err != nil {
		t.Errorf("err newA8Config %s", err)
	}

	a8Config, err := newA8Config(email, handle, pkPem)
	if err != nil {
		t.Errorf("err newA8Config %s", err)
	}

	/**** This works if we don't have this middle bit  ****/

	path := filepath.Join(a8dir, "test.yaml")
	defer os.Remove(path)

	err = createAccountFile(a8Config, path)
	if err != nil {
		t.Errorf("err PemToPublicKey %s", err)
	}

	a8ConfigRead, err := getA8ConfigFromFile(path)
	if err != nil {
		t.Errorf("err getA8ConfigFromFile %s", err)
	}

	pkPem2 := a8ConfigRead.Account.PrivateKeys[0]

	if !bytes.Equal(pkPem2, pkPem) {
		t.Errorf("pk is not surviving the trip")
	}

	/**** This works if we don't have this middle bit  ****/

	pk, err := a8Crypto.PemToPrivateKey(pkPem2)
	if err != nil {
		t.Errorf("err PemToPrivateKey %s", err)
	}

	pub, err := a8Crypto.PemToPublicKey(pubBytes)
	if err != nil {
		t.Errorf("err PemToPublicKey %s", err)
	}

	if !checkEqualPub(&pk.PublicKey, pub) {
		t.Errorf("err checkEqualPub, keys don't match!")
	}
}



func checkEqualPk(a, b *rsa.PrivateKey) bool {
	ma := x509.MarshalPKCS1PrivateKey(a)
	mb := x509.MarshalPKCS1PrivateKey(b)
	return bytes.Equal(ma, mb)
}

func checkEqualPub(a, b *rsa.PublicKey) bool {
	ma := x509.MarshalPKCS1PublicKey(a)
	mb := x509.MarshalPKCS1PublicKey(b)
	return bytes.Equal(ma, mb)
}
