package cli

import (
	"bufio"
	"fmt"
	"io"
	"os"

	a8Util "gitlab.com/autom8.network/go-a8-util"
)

func getStdIn() (string, error) {
	a8Util.Log.Info("HERE")
	stdIn, err := os.Stdin.Stat()
	if err != nil {
		return "", err
	}

	if stdIn.Mode()&os.ModeCharDevice != 0 {
		return "", nil
	}

	a8Util.Log.Debug("PIPE")
	reader := bufio.NewReader(os.Stdin)
	var output []rune

	for {
		input, _, err := reader.ReadRune()

		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)
	}

	a8Util.Log.Info("PRINT")
	var input string
	for j := 0; j < len(output); j++ {
		input += fmt.Sprintf("%c", output[j])
	}

	return input, nil
}
