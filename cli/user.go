package cli

import (
	"bytes"
	"encoding/json"
	"fmt"
	a8util "gitlab.com/autom8.network/go-a8-util"
	"io/ioutil"
	"net/http"
	urlNet "net/url"
)

//todo why is this here, shouldn't this be from a shared library or something. What is dyanmodb
type User struct {
	//ID string `json:"id"`
	Handle       string `json:"handle" dynamodbav:"Handle,omitempty"`
	EncryptedPK  []byte `json:"encryptedPK" dynamodbav:"EncryptedPK,omitempty"`
	PublicKey    string `json:"publicKey" dynamodbav:"PublicKey,omitempty"`
	Email        string `json:"email"  dynamodbav:"Email,omitempty"`
	CreateDate   int    `json:"createDate,omitempty" dynamodbav:"CreateDate,omitempty"`
	ModifiedDate int    `json:"modifiedDate,omitempty" dynamodbav:"ModifiedDate,omitempty"`
}

var httpUserURL = "https://ua.a8.dev"

//addUser adds a user to the remote server
func addUser(email string, handle string, pubBytes []byte, encryptedPK []byte) error {
	//for now generate the public/private key

	user := &User{
		PublicKey:   string(pubBytes),
		EncryptedPK: encryptedPK,
		Email:       email,
		Handle:      handle,
	}

	if err := PostUser(user); err != nil {
		return err
	}

	return nil
}

func GetUser(email string, handle string) (*User, error) {
	params := urlNet.Values{}
	params.Add("email",email)
	params.Add("handle",handle)

	url := fmt.Sprintf("/v1.0/users?%s", params.Encode())

	var user User

	body, statusCode, err := send(url, http.MethodGet, nil)

	if err != nil {
		return nil, err
	}

	if statusCode != 200 {
		return nil, fmt.Errorf("%s", string(body))
	}

	a8util.Log.Info("return data")
	a8util.Log.Info(string(body))
	a8util.Log.Info(statusCode)

	err = json.Unmarshal(body, &user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func PostUser(user *User) (err error) {
	if err := validatePost(user); err != nil {
		return err
	}

	userByte, err := json.Marshal(user)
	if err != nil {
		return err
	}

	url := "/v1.0/users"

	response, statusCode, err := send(url, http.MethodPost, userByte)

	a8util.Log.WithFields(a8util.Fields{
		"response":   string(response),
		"statusCode": statusCode,
		"err":        err,
	}).Info("got user response")

	//todo what happens to returns that don't work???
	//like if the value already exists?

	if err != nil {
		return err
	}

	if statusCode != 200 {
		return fmt.Errorf("%s", response)
	}

	return nil
}

func PutUser(user *User) error {
	url := "/v1.0/users/" + user.Handle

	userChanges := &User{
		Handle: user.Handle,
		Email:  user.Email,
	}

	userByte, err := json.Marshal(userChanges)
	if err != nil {
		return err
	}

	response, statusCode, err := send(url, http.MethodPost, userByte)

	if err != nil {
		return err
	}

	if statusCode != 200 {
		return fmt.Errorf("%s", response)
	}

	return nil
}

func send(url string, method string, dataRaw []byte) ([]byte, int, error) {

	client := &http.Client{}

	var body []byte

	fullURL := httpUserURL + url

	a8util.Log.WithFields(a8util.Fields{
		"url":        fullURL,
		"httpMethod": method,
		"data":       a8util.TruncateString(string(dataRaw), 1000),
	}).Info("Making user http call")

	var buf = bytes.NewBuffer(dataRaw)

	req, err := http.NewRequest(method, fullURL, buf)
	if err != nil {
		a8util.Log.Errorf("%s", err)
		return nil, 0, err
	}

	resp, err := client.Do(req)

	if err != nil {
		return nil, 0, fmt.Errorf("no user service found at %s", httpUserURL)
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	return body, resp.StatusCode, nil
}

func validatePost(user *User) error {
	if user.PublicKey == "" ||
		user.EncryptedPK == nil ||
		user.Email == "" ||
		user.Handle == "" {
		return fmt.Errorf("Please use format { email :  , handle : , encryptedPK : , publicKey :}, all fields must be complete")
	}

	return nil
}
