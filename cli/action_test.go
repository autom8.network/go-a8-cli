package cli

import (
	"crypto/rand"
	"crypto/rsa"
	"testing"
	daemonClient "gitlab.com/autom8.network/go-a8-http/client"
	)

var namespace = "a8.help"
var namespace2 = ""


var pk, pub = generateKeyPair(4096)


//TestShowNS tests down stream non file stuff
func TestShowNS(t *testing.T) {
	config := daemonClient.NewDaemonConfig("", "", 0, pk)

	daemonClient.Config = &config
	_, err := daemonClient.NamespaceInfo(namespace2)
	if err != nil {
		t.Errorf("err NamespaceInfo %s", err)
	}
}


func generateKeyPair(bits int) (*rsa.PrivateKey, *rsa.PublicKey) {
	privkey, _ := rsa.GenerateKey(rand.Reader, bits)

	return privkey, &privkey.PublicKey
}

