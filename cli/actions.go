package cli

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/fatih/color"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
	"text/tabwriter"
	"time"

	"github.com/fnproject/cli/common"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gopkg.in/yaml.v2"

	cliUtil "gitlab.com/autom8.network/go-a8-cli/util"
	daemonClient "gitlab.com/autom8.network/go-a8-http/client"
	a8Util "gitlab.com/autom8.network/go-a8-util"
	a8Crypto "gitlab.com/autom8.network/go-a8-util/crypto"
	"gitlab.com/autom8.network/go-a8-util/fqn"
	pb "gitlab.com/autom8.network/go-a8-util/proto"
)

const noFQNError = "🚨 Please specify a name in the format of 'namespace.function' where namespace and function contain no periods"
const repository = "testnetfns.a8.dev"

var bold = color.New(color.Bold)

//CreateAccount creates a user account, storing email and handle locally and sending a public and encrypted private key to a(8) servers
func CreateAccount(c *cli.Context) error {
	email := c.String("email")
	handle := c.String("handle")

	email = strings.ToLower(email)

	if email == "" || handle == "" {
		fmt.Println("Please use format: a8 create account --email [email], --handle [handle]")
		return nil
	}

	fmt.Println(`IMPORTANT - DO NOT SKIP READING
-------------------------------
The password you are about to enter encrypts your private key, which is stored on a(8)'s servers. When you log in to a(8)
you download your encryoted private key, decrypt it with your password, and then use your private key to work with a(8).`)

	if _, err := bold.Print("This means that a(8) does not store your password. "); err != nil {
		fmt.Print("***This means that a(8) does not store your password.***")
	}
	fmt.Println(`You are therefore responsible for keeping track of your password.
If you lose it, it cannot be recovered and it cannot be reset and we will not be able to help you. The consequence of 
this is that you will lose access to all your a(8) assets. We highly recommend that you use a password manager (we like 
1Password) to securely store your password.`)
	fmt.Print("Password: ")
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return err
	}

	fmt.Println("")

	fmt.Print("Verify password: ")
	byteVerifyPassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return err
	}

	fmt.Println("")

	if !bytes.Equal(byteVerifyPassword, bytePassword) {
		fmt.Println("Passwords don't match.")
		return nil
	}

	password := string(bytePassword)

	pkPem, encryptedPK, pubBytes, err := a8Crypto.CreateEncryptedPK(password)
	if err != nil {
		return err
	}

	//adds user to remote web db
	if err := addUser(email, handle, pubBytes, encryptedPK); err != nil {
		fmt.Print(err)
		return nil
	}

	if err := writeConfigs(email, handle, pkPem); err != nil {
		return err
	}

	pemPath, err := a8PkPemFilePath(handle)
	if err != nil {
		return err
	}

	fmt.Println("Private key created at " + pemPath)

	if _, err := bold.Print("Please back this up right now. "); err != nil {
		fmt.Print("***Please back this up right now.***")
		return nil
	}

	fmt.Println("If you lose your password you can still use your backed up private key to work with a(8).")

	return nil
}

//SwitchAccount switches between existing accounts
func SwitchAccount(c *cli.Context) error {
	email := c.String("email")
	handle := c.String("handle")

	email = strings.ToLower(email)

	if email == "" && handle == "" {
		fmt.Println("Please include either email or handle: a8 login --email [email], --handle [handle]")
		return nil
	}

	user, err := GetUser(email, handle)
	if err != nil {
		fmt.Printf("Please try a different handle or email. Error: %s \n ", err)
		return nil
	}

	fmt.Println("Password: ")
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return err
	}

	password := string(bytePassword)

	pkPem, err := a8Crypto.DecryptData(password, user.EncryptedPK)
	if err != nil {
		return err
	}

	if err := writeConfigs(user.Email, user.Handle, pkPem); err != nil {
		return err
	}

	fmt.Printf("Switched account to %s %s \n", handle, email)
	return nil
}

// InvokeAction runs a function with a given input on a8
func InvokeAction(c *cli.Context) error {
	if err := setupDaemonClient(); err != nil {
		return err
	}

	var input []byte

	stdIn, err := getStdIn()
	if err == nil {
		a8Util.Log.Debug("Problem with stdin")
	}

	if len(c.Args()) == 0 {
		errorMessage := noFQNError
		return errors.New(errorMessage)
	}

	fullyQualifiedName := c.Args()[0]

	if len(c.Args()) > 1 {
		input = []byte(c.Args()[1])
	} else if len(stdIn) != 0 {
		input = []byte(stdIn)
	}

	a8Util.Log.Info(input)
	start := time.Now()

	a8Util.Log.WithFields(logrus.Fields{
		"fullyQualifiedName": fullyQualifiedName,
		"input":              input,
	}).Info("about to invoke")

	output, err := daemonClient.Invoke(fullyQualifiedName, input)
	if err != nil {
		return err
	}

	a8Util.Log.WithFields(logrus.Fields{
		"Time Took": time.Since(start),
	}).Info("returned from invoke")

	printInvokeOutput(output)

	return nil
}

//ShowNamespace Shows all the child namespaces at any point on the tree
func ShowNamespace(c *cli.Context) error {
	if err := setupDaemonClient(); err != nil {
		return err
	}

	var nameSpace string

	if len(c.Args()) > 0 {
		nameSpace = c.Args()[0]
	}

	namespaceData, err := daemonClient.NamespaceInfo(nameSpace)
	if err != nil {
		return err
	}

	switch namespaceData.Response.(type) {
	case *pb.ShowResponse_FnFunctionData:
		err = prettyPrintJson(namespaceData.GetFnFunctionData())

	case *pb.ShowResponse_NamespaceList:
		err = displayFnList(namespaceData.GetNamespaceList().List)

	default:
		fmt.Println("The ns you're looking for doesn't exist. Please try again or correct the namespace format.")
		fmt.Println("Examples:")
		fmt.Println("a8 show ns")
		fmt.Println("a8 snow ns <namespace>")
		fmt.Println("a8 show ns <namespace>.<fn>")
	}

	if err != nil {
		return err
	}

	return nil
}

// BuildAction builds a local docker image
func BuildAction(c *cli.Context) error {
	_, err := Build(c)
	return err
}

// Bump bumps up the version
func Bump(c *cli.Context) error {
	versionBump := 2

	if c.Bool("major") {
		versionBump = 0
	}

	if c.Bool("minor") {
		versionBump = 1
	}

	newVersion, err := bumpFnVersion(versionBump)
	if err != nil {
		return err
	}

	fmt.Printf("New version is: %s \n", newVersion)

	return nil
}

// DeployAction Bumps the version, Builds a Docker image, Pushes image Docker Hub (if a repository is supplied), Updates database with function information
func DeployAction(c *cli.Context) error {
	if err := setupDaemonClient(); err != nil {
		return err
	}

	configs := getConfigsFromYaml()

	//todo more advanced logic around versioning
	newVersion, err := bumpFnVersion(2)
	if err != nil {
		return err
	}

	fmt.Printf("New version is: %s \n", newVersion)

	imageName, err := Build(c)
	if err != nil {
		a8Util.Log.Fatal("failed to build image ", err)
	}

	fmt.Printf("Deploying Image ")

	shutdownCh := make(chan struct{})
	go indicator(shutdownCh)

	if repository != "" {
		err := Push(imageName)
		if err != nil {
			return err
		}
	}

	if err = update(configs, imageName); err != nil {
		return err
	}

	close(shutdownCh)
	fmt.Print("\n")

	if err != nil {
		fmt.Printf("Oh no! Something's gone wrong. %s \n", err)
		return err
	}

	var invokeCommandList [][]string

	invokeCommandList = append(invokeCommandList, []string{"CLI", fmt.Sprintf("a8 invoke %s [functionInputJson] \n", configs.Name)})
	invokeCommandList = append(invokeCommandList, []string{"Slack", fmt.Sprintf("/a8 %s <key>:<value>... \n", configs.Name)})
	invokeCommandList = append(invokeCommandList, []string{"Discord", fmt.Sprintf("a8! %s <key>:<value>... \n", configs.Name)})

	w := tabwriter.NewWriter(os.Stdout, 0, 8, 3, ' ', 0)
	for _, cmdList := range invokeCommandList {
		_, err = fmt.Fprint(w, "\t", cmdList[0], "\t", cmdList[1])
		if err != nil {
			fmt.Println("there was an error!")
			return err
		}
	}

	if err := w.Flush(); err != nil {
		return err
	}

	return nil
}

// InitAction creates a new function boilerplate
func InitAction(c *cli.Context) error {
	runtime := c.String("runtime")
	timeout := c.Int("timeout")
	idleTimeout := c.Int("idle-timeout")
	memory := c.Int("memory")
	folder := c.String("folder")
	slack := c.Bool("slack")
	discord := c.Bool("discord")
	sheets := c.Bool("sheets")

	if len(c.Args()) == 0 {
		errorMessage := "Please include a function name!"

		return errors.New(errorMessage)
	}

	functionName := c.Args()[0]

	configPath, err := a8configFilePath()
	if err != nil {
		return err
	}

	a8Config, err := getA8ConfigFromFile(configPath)
	if err != nil {
		return err
	}

	fullyQualifiedName := a8Config.Account.Handle + "." + functionName

	err = fqn.Validate(fullyQualifiedName)
	if err != nil {
		return err
	}

	_, fnName, err := fqn.Parse(fullyQualifiedName)
	if err != nil {
		return err
	}

	if runtime != "node" {
		errorMessage := "'node' is the only runtime currently available"
		return errors.New(errorMessage)
	}

	//finding current folder position
	dir, err := os.Getwd()
	if err != nil {
		a8Util.Log.Fatal(err)
	}

	if folder == "" {
		folder = fnName
	}

	dirSlice := strings.Split(dir, "/")
	currentFolder := dirSlice[len(dirSlice)-1]

	//if we're in the correct folder we need not append it to the current directory
	if folder == currentFolder {
		folder = ""
	}

	path := dir

	if folder != "" {
		path = filepath.Join(dir, folder)
		err = os.Mkdir(folder, os.ModePerm)
		if err != nil {
			return err
		}
	}

	err = createNodeBoilerplate(path, slack, discord, sheets)
	if err != nil {
		return err
	}

	//todo variableize this
	newFnConfig := fnConfig{
		SchemaVersion: 20180708,
		Name:          fullyQualifiedName,
		Version:       "0.0.1",
		Runtime:       runtime,
		EntryPoint:    "node func.js",
		Format:        "http-stream",
		Timeout:       int32(timeout),
		IdleTimeout:   int32(idleTimeout),
		Memory:        int32(memory),
	}

	newConfigBytes, _ := yaml.Marshal(newFnConfig)

	pathToFuncYaml := filepath.Join(path, "func.yaml")

	if exists(pathToFuncYaml) {
		return nil
	}

	err = ioutil.WriteFile(pathToFuncYaml, newConfigBytes, 0644)
	if err != nil {
		return err
	}

	fmt.Println("Function boilerplate generated.")

	return nil
}

// createNodeBoilerplate creates a local boilderplate for a node function
func createNodeBoilerplate(path string, slack bool, discord bool, sheets bool) error {

	a8Util.Log.Info("in create node boilderplate")

	funcJsContent := `const fdk=require('@autom8/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  let name = 'World';
  if (input.name) {
    name = input.name;
  }
  return {'message': 'Hello ' + name}
})
`

	if slack {
		funcJsContent += `

fdk.slack(function(result){
	return {
        "response_type": "in_channel",
        "blocks" : [
			{
				"type": "section",
				"text": {
					"type": "mrkdwn",
					"text": "hello world in slack form! \n <https://api.slack.com/tools/block-kit-builder| Click here> to see what you can do with Slack formatting!"
				}
			}
		]
      }
})
`
	}

	if discord {
		funcJsContent += `

fdk.discord(function(result){
    return {
        "content" : result.message + " in discord form!",
        "embed" : {
          "description" : "[Click here](https://leovoel.github.io/embed-visualizer/) for an example of what you can do in Discord responses."
        }
    }
})
`
	}

	if sheets {
		funcJsContent += `

fdk.sheets(function(result) {
    const sheets = []
    sheets.push(["Header 1","Header 2"])
    sheets.push(["data 1","data 2"])
    return {sheets}
})
`
	}

	const packageJsonContent = `{
	"name": "helloa8",
    "version": "1.0.0",
	"description": "example a8 function",
	"main": "func.js",
	"author": "",
	"license": "Apache-2.0",
	"dependencies": {
		"@autom8/fdk": "^0.2.5",
    	"@autom8/js-a8-fdk": "^0.3.0"
	}
}
`

	pathToPackageJsonFile := filepath.Join(path, "package.json")
	pathToFuncJs := filepath.Join(path, "func.js")

	if exists(pathToPackageJsonFile) || exists(pathToFuncJs) {
		return nil
	}

	a8Util.Log.Info(fmt.Sprintf("the path to pack is %s", pathToPackageJsonFile))

	err := ioutil.WriteFile(pathToPackageJsonFile, []byte(packageJsonContent), os.FileMode(0644))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(pathToFuncJs, []byte(funcJsContent), os.FileMode(0644))
	if err != nil {
		return err
	}
	return nil
}

// Push pushes a local docker image to a repository based on local docker credentials
func Push(imageName string) error {
	/*
		Passing registry credentials around is unintuitive and not what a user wants to be doing.
		To allow a use to just push using their existing docker setup, we're going to do push as a docker command
	*/

	a8Util.Log.Info(fmt.Sprintf("Pushing %v to docker registry...", strings.SplitN(imageName, "/", 2)[1]))
	cmd := exec.Command("docker", "push", imageName)
	err := cmd.Run()

	if err != nil {
		errMsg := fmt.Sprintf("error running docker push, are you logged into docker?: %v", err)
		return errors.New(errMsg)
	}

	return nil
}

// Build kicks off the command to build a docker image
func Build(c *cli.Context) (imageName string, err error) {
	dir := func() string {
		var dir string
		if c.String("working-dir") != "" {
			dir = c.String("working-dir")
		} else {
			dir = func() string {
				wd, err := os.Getwd()
				if err != nil {
					a8Util.Log.Error("Couldn't get working directory:", err)
				}
				return wd
			}()
		}

		return dir
	}()

	err = os.Chdir(dir)
	if err != nil {
		return imageName, err
	}
	defer os.Chdir(dir)

	fpath, ff, _ := common.FindAndParseFuncFileV20180708(dir)

	_, err = common.BuildFuncV20180708(cliUtil.Verbose, fpath, ff, []string{}, true)
	if err != nil {
		return imageName, err
	}

	imageName = ff.ImageNameV20180708()

	//now rename the image if there is a repository string

	if repository != "" {
		imageName = fmt.Sprintf("%s/%s:%s", repository, ff.Name, ff.Version)

		a8Util.Log.Info("renaming image")
		err = cliUtil.DockerCli.ImageTag(context.Background(), ff.ImageNameV20180708(), imageName)
		if err != nil {
			return imageName, nil
		}
	}

	return imageName, nil
}
